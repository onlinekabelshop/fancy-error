import FancyError from '../src/fancy-error';
import { expect } from 'chai';

const testErrorMessage = 'This is a primitive error';
const testError = new Error(testErrorMessage);

class OtherError extends FancyError {}

describe('FancyError unit tests', (): void => {
  it('new FancyError: message', (): void => {
    const message = 'This is a test message';
    const error = new FancyError(message);
    expect(error.message).to.equal(message);
  });
  it('new FancyError: fullMessage', (): void => {
    const message = 'This is a test message';
    const error = new FancyError(message);
    expect(error.fullMessage).to.equal(message);
  });

  it('new FancyError(caughtError): caughtError.message', (): void => {
    const error = new FancyError('test', testError);
    expect(error.caughtError.message).to.equal(testErrorMessage);
  });
  it('new FancyError(caughtError): fullMessage (1)', (): void => {
    const error = new FancyError('test', testError);
    expect(error.fullMessage.includes(testErrorMessage)).to.equal(true);
  });
  it('new FancyError(caughtError): fullMessage (2)', (): void => {
    const error = new FancyError(testErrorMessage, new Error('test'));
    expect(error.fullMessage.includes(testErrorMessage)).to.equal(true);
  });
  it('new FancyError(caughtError): fullMessage (3)', (): void => {
    const error = new FancyError('test', testError);
    expect(error.fullMessage.includes(`[${testError.name}]`)).to.equal(true);
  });

  it('(new FancyError).name = FancyError', (): void => {
    const error = new FancyError('test');
    expect(error.name).to.equal('FancyError');
  });
  it('(new OtherError).name = OtherError', (): void => {
    const error = new OtherError('test');
    expect(error.name).to.equal('OtherError');
  });

  it('new FancyError is instanceof FancyError', (): void => {
    const error = new FancyError('test');
    expect(error instanceof FancyError).to.equal(true);
  });
  it('new OtherError is instanceof FancyError', (): void => {
    const error = new OtherError('test');
    expect(error instanceof FancyError).to.equal(true);
  });
});
