export default class FancyError extends Error {
  public readonly caughtError?: FancyError | Error;
  public readonly context?: { [key: string]: any };

  constructor(
    message: string,
    caughtError?: FancyError | Error,
    context?: typeof FancyError.prototype.context,
    allowPassthrough: boolean = true
  ) {
    super(message);
    this.name = this.constructor.name;

    if (
      caughtError &&
      caughtError instanceof FancyError &&
      caughtError.name == this.name &&
      allowPassthrough
    ) {
      return caughtError;
    }

    if (caughtError) this.caughtError = caughtError;
    if (context != undefined) this.context = context;
  }

  public get fullMessage(): string
  {
    if (!this.caughtError) return this.message;

    let caughtMessage =
      this.caughtError instanceof FancyError
      ? this.caughtError.fullMessage
      : this.caughtError.message;

    return `${this.message}: [${this.caughtError.name}] ${caughtMessage}`;
  }
}
